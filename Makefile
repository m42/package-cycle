FILES=package-cycle.svg archive-software.svg

.PHONY: all
all: $(FILES)

%.svg: %.dot
	gdot -Tsvg $< > $@

.PHONY: clean
clean:
	rm -f $(FILES)
